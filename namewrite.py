#!/usr/bin/env python

import signal
import time
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
from pirc522 import RFID

CONST_FNAME = 12
CONST_LNAME = 13
CONST_LOGIN = 14
CONST_PASS  = 15


run = True
rdr = RFID()
util = rdr.util()
util.debug = True



def read_user_data(title):
    name = raw_input("Please enter " + title + ": ")
    return name



def convert_ascii_to_int_list(ascii):
    chars = list(ascii)
    int_list = list()
    for char in chars:
        int_list.append(ord(char))
    return int_list
    

def convert_int_list_to_ascii(int_list):

    chars = list()
    for integer in int_list:
        chars.append(str(unichr(integer)))

    return ''.join(chars)

def write_to_card(sector, bytes):
    global util
    util.rewrite(sector, bytes)
    return True

# ------------------------   

def end_read(signal,frame):
    global run
    print("\nCtrl+C captured, ending read.")
    run = False
    rdr.cleanup()


signal.signal(signal.SIGINT, end_read)

print("Please touch to enter your name")

while run:
    (error, data) = rdr.request()
    #if not error:
    #    print("\nDetected: " + format(data, "02x"))

    (error, uid) = rdr.anticoll()
    if not error:
    #    print("Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))

    #    print("Set tag")
        util.set_tag(uid)
     #   print("\nAuthorizing")
        util.auth(rdr.auth_a, [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])


        name = read_user_data('Name')
        surname = read_user_data('Surname')
        age = read_user_data('Age')

       

        write_to_card(CONST_NAME, convert_ascii_to_int_list(name))
        write_to_card(CONST_SURNAME, convert_ascii_to_int_list(surname))
        write_to_card(CONST_AGE, convert_ascii_to_int_list(age))


        #print("\nWriting zero bytes")
        #util.rewrite(2, [None, None, 0, 0, 0])
        #util.read_out(2)
        #print("\nDeauthorizing")
        #util.deauth()


        util.write_trailer(1, (0x12, 0x34, 0x56, 0x78, 0x96, 0x92), (0x0F, 0x07, 0x8F), 105, (0x74, 0x00, 0x52, 0x35, 0x00, 0xFF))
        util.deauth()

        run = False
        print("Saved")
        time.sleep(1)


