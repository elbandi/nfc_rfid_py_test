#!/usr/bin/env python

import signal
import time
import os

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

from pirc522 import RFID

CONST_FNAME = 12
CONST_LNAME = 13
CONST_LOGIN = 14
CONST_PASS  = 15

run = True
rdr = RFID()
util = rdr.util()
util.debug = True


def displayIdleMenu():
    os.system('clear')  # For Linux/OS X
    print("\n1. Read card");
    print("\n2. Write card");
    print("\n3. CLose");
    idleTitle = raw_input('Choose menu:');
    return idleTitle

def read_user_data(title):
    name = raw_input("Please enter " + title + ": ")
    return name



def convert_ascii_to_int_list(ascii):
    chars = list(ascii)
    int_list = list()
    for char in chars:
        int_list.append(ord(char))
    return int_list
    

def convert_int_list_to_ascii(int_list):

    chars = list()
    for integer in int_list:
        chars.append(str(unichr(integer)))

    return ''.join(chars)

def write_to_card(sector, bytes):
    global util
    return util.rewrite(sector, bytes)


# ------------------------   

def end_read(signal,frame):
    global run
    print("\nCtrl+C captured, ending read.")
    run = False
    rdr.cleanup()

def read(util, block_address):
    
    if not util.is_tag_set_auth():
        return True

    error = util.do_auth(block_address)
    if not error:
        (error, data) = util.rfid.read(block_address)
        return convert_int_list_to_ascii(data)
    else:
        return "READ ERROR"


def print_msg(text, seconds):
     print(text)
     for num in range(1,seconds):  #to iterate between 10 to 20
        time.sleep(1)
        print('.')


signal.signal(signal.SIGINT, end_read)



idle = True
while idle:
    run_write = False
    run_read = False

    if idle:
        idleMenu = displayIdleMenu()
        if idleMenu == '1':
            run_read = True
        elif idleMenu == '2':
            run_write = True
        elif idleMenu == '3':
            idle = False
        else:
            print_msg('Wrong Menu',6)


    while run_write:
        print("\nPlease touch to rewrite card data")
        (error, data) = rdr.request()
        #if not error:
        #    print("\nDetected: " + format(data, "02x"))

        (error, uid) = rdr.anticoll()
        if not error:
            #    print("Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))

            #    print("Set tag")
            util.set_tag(uid)
            #   print("\nAuthorizing")
            util.auth(rdr.auth_a, [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])


            name = read_user_data('Name')
            surname = read_user_data('Surname')
            age = read_user_data('Age')


            write_to_card_error = False
            write_to_card_error = write_to_card(CONST_FNAME, convert_ascii_to_int_list(name))
            print(write_to_card_error);
            write_to_card_error = write_to_card(CONST_LNAME, convert_ascii_to_int_list(surname))
            print(write_to_card_error);
            write_to_card_error = write_to_card(CONST_LOGIN, convert_ascii_to_int_list(age))
            print(write_to_card_error);

            #print("\nWriting zero bytes")
            #util.rewrite(2, [None, None, 0, 0, 0])
            #util.read_out(2)
            #print("\nDeauthorizing")
            #util.deauth()


            util.write_trailer(1, (0x12, 0x34, 0x56, 0x78, 0x96, 0x92), (0x0F, 0x07, 0x8F), 105, (0x74, 0x00, 0x52, 0x35, 0x00, 0xFF))
            util.deauth()

            run_write = False
            print_msg('Saved',6)


    while run_read:
        print("\nPlease touch to be identified")

        (error, data) = rdr.request()
        #if not error:
        #    print("\nDetected: " + format(data, "02x"))

        (error, uid) = rdr.anticoll()
        if not error:
            print("Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))

        #    print("Set tag")
            util.set_tag(uid)
         #   print("\nAuthorizing")
            util.auth(rdr.auth_a, [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])


            name = read(util, CONST_FNAME);
            surname = read(util, CONST_LNAME);
            age = read(util, CONST_LOGIN);

            print("Good morning " + name + " " + surname + " (" + age + ") ")

            #print("\nWriting zero bytes")
            #util.rewrite(2, [None, None, 0, 0, 0])
            #util.read_out(2)
            #print("\nDeauthorizing")
            #util.deauth()


            util.write_trailer(1, (0x12, 0x34, 0x56, 0x78, 0x96, 0x92), (0x0F, 0x07, 0x8F), 105, (0x74, 0x00, 0x52, 0x35, 0x00, 0xFF))
            util.deauth()

            run_read = False
            print_msg('Closing',6)


